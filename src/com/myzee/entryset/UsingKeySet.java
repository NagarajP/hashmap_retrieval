package com.myzee.entryset;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class UsingKeySet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Integer, String> h = new HashMap<>();
		h.put(1, "one");
		h.put(2, "two");
		h.put(3, "three");
		
		System.out.println("java1.7 and Before");
		Set<Integer> s = h.keySet();
		Iterator<Integer> itr = s.iterator();
		while (itr.hasNext()) {
			Integer i = itr.next();
			System.out.println("key - " + i + " value - " + h.get(i));
		}
		
		System.out.println("\nAfter Java1.8\n");
		s.forEach(key -> System.out.println(key + " - "+ h.get(key) ));
	}
}