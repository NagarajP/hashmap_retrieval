package com.myzee.entryset;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class UsingEntrySet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Integer, String> h = new HashMap<>();
		h.put(1, "one");
		h.put(2, "two");
		h.put(3, "three");
		
		System.out.println("Java1.7 and before");
		Set<Entry<Integer, String>> s = h.entrySet();
		Iterator<Entry<Integer, String>> itr = s.iterator();
		while (itr.hasNext()) {
			Map.Entry<Integer, String> i = itr.next();
			System.out.print(i.getKey() + " = ");
			System.out.println(i.getValue());
//			i.setValue("dfdfd");
		}
		
		// Java1.8
		System.out.println("\nUsing Java1.8");
		h.entrySet().forEach((Map.Entry<Integer, String> e) -> System.out.println(e.getKey() + " = "+ e.getValue()));
	}

}
